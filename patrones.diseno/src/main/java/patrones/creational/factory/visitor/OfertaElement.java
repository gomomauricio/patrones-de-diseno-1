package patrones.creational.factory.visitor;

public interface OfertaElement
{

    void accept(CreditCardVisitor visitor);
}
