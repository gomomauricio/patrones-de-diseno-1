package patrones.creational.factory.interpreter;

/**
 *
 */
public class MainInterpreter {

    public static void main(String[] args) {

        Expression cero = new TerminalExpression("0");
        Expression uno = new TerminalExpression("1");

        Expression contieneBoolean = new OrExpression(cero,uno);
        Expression containsOneAndZero = new AndExpression(cero,uno);

        System.out.println( contieneBoolean.interpret("cero") );
        System.out.println( contieneBoolean.interpret("0") );

        System.out.println( containsOneAndZero.interpret("0") );
        System.out.println( containsOneAndZero.interpret("0, 1") );
    }
}
