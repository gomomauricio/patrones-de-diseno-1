package patrones.creational.factory.state;

public interface MobileAlertState {

    void alert(MobileAlertStateContex contex);
}
