package patrones.creational.factory.bridge;

public interface ICreditCard {

    void realizarPago();
}
