package patrones.creational.factory.prototype;

import java.util.HashMap;
import java.util.Map;

public class PrototypeFactory
{
    public static class CardType
    {
        public static final String VISA = "visa";
        public static final String AMEX = "amex";
    }

    private static Map<String,PrototypeCard> prototypes = new HashMap<>();

    public static PrototypeCard getInstance(final String tipo) throws  CloneNotSupportedException
    {
        return prototypes.get(tipo).clone();
    }

    public static void loadCard()
    {

        Visa visa = new Visa();
        visa.setName("Estat tarjeta es Visa con numero 0001");
        prototypes.put(CardType.VISA,visa);

        Amex amex = new Amex();
        amex.setName("Estat tarjeta es Amex con numero 0002");
        prototypes.put(CardType.AMEX,amex);
    }
}
