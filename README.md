# Código del curso Patrones de Diseño Java
# Instructor
> [Alberto Palomar](https://www.udemy.com/course/patrones-de-diseno-java/) 
 

# Udemy
* Patrones de Diseño Java

## Contiene

* **Patrones Creacionales** 

* * Factory Method
* * Abstract Factory
* * Builder
* * Prototype
* * Singlenton


* **Patrones de Comportamiento**  

* * Chain of Responsibility
* * Command
* * Interpreter
* * Iteractor
* * Mediator
* * Memento
* * Obsever
* * State
* * Strategy
* * Template method
* * Visitor

* **Patrones Estructurales** 

* * Adapter
* * Bridge
* * Composite
* * Decorator
* * Facade
* * Flyweight
* * Proxy


## Descripción
   
Los patrones de diseños son unas técnicas para resolver problemas comunes que nos ocurren a todos los desarrolladores de software.

Un patrón de diseño es una solución a un problema de diseño que ya ha sido resuelto por anterioridad y que nosotros solo debemos implementar.
 
Patrones Creacionales: Son los patrones de diseño que nos solucionan nuestros problemas a la hora de crear instancias.
 
Patrones de Comportamiento: Son los patrones de diseño que se encargan de definir las formas en las que interactúan y reparten responsabilidades las distingas clases y objetos.
 
Patrones Estructurales: Son los patrones de diseño que tratan la composición de las clases y objetos.
 
---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

/************* MAVEN ***********************/
 
 *  


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 18 FEB, 2022 ]  
 
 