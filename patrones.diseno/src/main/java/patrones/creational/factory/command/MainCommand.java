package patrones.creational.factory.command;

/*
Encapsula todo la informacion necesaria para realizar una accion
o  desencadenar un evento en cualquier momento.

Permite desacoplar una accion para que otra clase la ejecute
 */


public class MainCommand {

    public static void main(String[] args) {

        CreditCard creditCard = new CreditCard();
        CreditCard creditCardDesactivate = new CreditCard();

        CreditCardInvoker invoker = new CreditCardInvoker();
        invoker.setCommand( new CreditCardActivateCommand(creditCard));
        invoker.run();
        System.out.println("----------------------------");
        invoker.setCommand( new CreditCardDesactivateCommand(creditCard));
        invoker.run();

        System.out.println("----------- FINALIZA  -----------------");

    }
}
