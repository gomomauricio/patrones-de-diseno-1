package patrones.creational.factory.state;

public class MobileAlertStateContex {

    private MobileAlertState currentState;

    public MobileAlertStateContex()
    {
        currentState = new Sound();
    }

    public void setState(MobileAlertState state)
    {
         currentState = state;
    }

    public void alert()
    {
        currentState.alert(this);
    }

}
