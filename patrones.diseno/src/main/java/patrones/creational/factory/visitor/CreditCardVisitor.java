package patrones.creational.factory.visitor;

public interface CreditCardVisitor {

    void ofertaGasolina(OfertaGasolina gasolina);

    void ofertaVuelos(OfertaVuelos vuelos);

}
