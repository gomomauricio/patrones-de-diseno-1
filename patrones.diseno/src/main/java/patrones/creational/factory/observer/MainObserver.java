package patrones.creational.factory.observer;

/**
 * cuando se necesita notificar a varios objetos un cambio de estado
 */
public class MainObserver {


    public static void main(String[] args) {
        Coche coche = new Coche();
        Peaton peaton = new Peaton();

        MessagePublisher msg = new MessagePublisher();

        msg.attach(coche);
        msg.attach(peaton);

        msg.notifyUpdate( new Semaforo("ROJO COCHE"));

        try {
            Thread.sleep(2000);
        }
        catch (Exception e)
        {

        }

        msg.notifyUpdate(new Semaforo("VERDE COCHE"));
    }
}
