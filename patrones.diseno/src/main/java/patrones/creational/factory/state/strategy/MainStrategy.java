package patrones.creational.factory.state.strategy;

/*
Permite elegir algoritmo a usar en tiempo de ejecucion
 */
public class MainStrategy {

    public static void main(String[] args) {

        Context context = new Context(new CapitalStartegyTextFormatter());

        context.publishText("Este texto va en mayusculas");

        context = new Context( new LowerStrategyTestFormatter());

        context.publishText("ESTE TextO SerA En MiNusCulas");

    }
}
