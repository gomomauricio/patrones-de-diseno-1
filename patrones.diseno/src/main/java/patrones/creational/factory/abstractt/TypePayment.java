package patrones.creational.factory.abstractt;

public enum TypePayment {
    CARD,
    GOOGLEPAY
}
