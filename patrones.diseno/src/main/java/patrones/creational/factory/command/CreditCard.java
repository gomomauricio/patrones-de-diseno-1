package patrones.creational.factory.command;

public class CreditCard {

    public void sendPinNumberToCustumer()
    {
        System.out.println("El pin number ha sido enviado al cliente.");
    }

    public void sendSMSToCustumerActivate()
    {
        System.out.println("Enviando SMS al cliente informando que su tarjeta ha sido activada.");
    }

    public void activate()
    {
        System.out.println("La tarjeta a sido activada.");
    }

    public void desactivate()
    {
        System.out.println("La tarjeta a sido desactivada.");
    }

    public void sendSMSToCustumerDesactivate()
    {
        System.out.println("Enviando SMS al cliente informando que su tarjeta ha sido desactivada.");
    }






}
