package patrones.creational.factory.state;

public class Vibration implements MobileAlertState{
    @Override
    public void alert(MobileAlertStateContex contex) {
        System.out.println("Vibrando . . . Vibrando . . . ");
    }
}
