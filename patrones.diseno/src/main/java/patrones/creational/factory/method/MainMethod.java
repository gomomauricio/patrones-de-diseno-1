package patrones.creational.factory.method;

/*
Define la estructura del algoritmo, es una plantilla
 */
public class MainMethod {
    public static void main(String[] args) {

        Payment payment = new Visa();
        payment.makePayment();

        payment = new Paypal();
        payment.makePayment();


    }
}
