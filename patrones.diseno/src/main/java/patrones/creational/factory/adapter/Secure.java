package patrones.creational.factory.adapter;

public interface Secure {
    void payWithSecureLevelA();
    void payWithSecureLevelZ();
}
