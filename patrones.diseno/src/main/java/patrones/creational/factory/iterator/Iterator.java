package patrones.creational.factory.iterator;

public interface Iterator {

    boolean hasNext(); //para saber si hay mas elementos  o no

    Object next();

    Object currectItem();

}
