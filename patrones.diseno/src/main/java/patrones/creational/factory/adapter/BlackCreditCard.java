package patrones.creational.factory.adapter;

public class BlackCreditCard implements Secure{
    @Override
    public void payWithSecureLevelA() {
        System.out.println("Tarjeta BLACK: Pagando con seguridad A MAXIMO");
    }

    @Override
    public void payWithSecureLevelZ() {


    }
}
