package patrones.creational.factory.composite;

/*
permite hacer objetos complejos a partir de onjetos mas simples
 */
public class MainComposite {
    public static void main(String[] args) {
        CuentaComponent cuentaCorriente = new CuentaCorriente(1000.0,"Alberto");
        CuentaComponent cuentaAhorro = new CuentaAhorro(20000.0,"Alberta");

        CuentaComposite cuentaComposite = new CuentaComposite();

        cuentaComposite.addCuenta( cuentaCorriente );
        cuentaComposite.addCuenta( cuentaAhorro );

        cuentaComposite.showAccountName();
        System.out.println( "$ " + cuentaComposite.getAmount() );

    }
}
