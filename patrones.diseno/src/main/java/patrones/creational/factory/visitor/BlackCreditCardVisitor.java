package patrones.creational.factory.visitor;

public class BlackCreditCardVisitor implements CreditCardVisitor
{
    @Override
    public void ofertaGasolina(OfertaGasolina gasolina) {
        System.out.println("Descuento del 10% de gasolina con tu tarjeta clasica");
    }

    @Override
    public void ofertaVuelos(OfertaVuelos vuelos) {
        System.out.println("Descuento del 25% de vuelos con tu tarjeta clasica");
    }
}