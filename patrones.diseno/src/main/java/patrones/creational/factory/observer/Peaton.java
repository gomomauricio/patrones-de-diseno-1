package patrones.creational.factory.observer;

public class Peaton implements Observer
{


    @Override
    public void update(Semaforo semaforo) {
        if(semaforo.estatus.equals("ROJO COCHE"))
        {
            System.out.println("Semaforo PEATON verde: peaton puede pasar");
        }else
        {
            System.out.println("Semaforo PEATON rojo: peaton NO puede pasar");
        }

    }
}
