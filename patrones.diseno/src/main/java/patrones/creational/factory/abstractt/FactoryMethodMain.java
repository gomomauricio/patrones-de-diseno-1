package patrones.creational.factory.abstractt;

public class
FactoryMethodMain {

	public static void main(String[] args) {

		System.out.println("Factory Method ");
		System.out.println(" Crear Instancias de objetos, que no sabemos de que tipo va hacer");
		probarFactoryMethod(TypePayment.CARD);
		System.out.println(" - - - - - - ");
		probarFactoryMethod(TypePayment.GOOGLEPAY);
	}


	private static  void  probarFactoryMethod(TypePayment type)
	{
	//	Payment payment = PaymentFactory.builPayment(TypePayment.CARD);
		Payment payment = PaymentFactory.builPayment(type);
		payment.doPayment();

	}


}
