package patrones.creational.factory.mediator;




public class MainMetiator {

    public static void main(String[] args) {

        ConcreteMediator mediator = new ConcreteMediator();
        ConcreteCollage1 user1 = new ConcreteCollage1(mediator);
        ConcreteCollage2 user2 = new ConcreteCollage2(mediator);

        mediator.setUser1(user1);
        mediator.setUser2(user2);

        user1.send( "  Hola soy user1" );
        user2.send( "  Hola user1, soy user3" );


    }

}
