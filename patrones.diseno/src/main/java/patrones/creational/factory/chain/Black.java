package patrones.creational.factory.chain;

public class Black  implements ApproveLoanChain
{
    private ApproveLoanChain next;

    @Override
    public void setNext(ApproveLoanChain loan) {
        next = loan;
    }

    @Override
    public ApproveLoanChain getNext() {
        return next;
    }

    @Override
    public void crediCardRequest(int totalLoan) {
        if(totalLoan >  50000)
        {
            System.out.println("Esta solicitud de tarjeta la maneja la tarjeta Black");
        }
        else
        {
            //pasamos a la siguiente tarjeta
            next.crediCardRequest(totalLoan);
        }
    }
}
