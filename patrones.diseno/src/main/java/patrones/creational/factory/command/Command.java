package patrones.creational.factory.command;

public interface Command {

    void execute();
}
