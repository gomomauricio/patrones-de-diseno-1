package patrones.creational.factory.mediator;

public interface Mediator {

    void send(String message, Collage collage);
}
