package patrones.creational.factory.abstractt;

public class
FactoryAbstractMethodMain {

	public static void main(String[] args) {

		System.out.println("Factory Method ");
		System.out.println(" Crear Instancias de factories class,una fabrica de fabricas de objetos");
		probarFactoryMethod(TypePayment.CARD);
		System.out.println(" - - - - - - ");
		probarFactoryMethod(TypePayment.GOOGLEPAY);
	}


	private static  void  probarFactoryMethod(TypePayment type)
	{
	//	Payment payment = PaymentFactory.builPayment(TypePayment.CARD);
		Payment payment = PaymentFactory.builPayment(type);
		payment.doPayment();

	}


}
