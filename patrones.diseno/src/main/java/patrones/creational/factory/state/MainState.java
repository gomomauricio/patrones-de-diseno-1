package patrones.creational.factory.state;

/**
 * Nos ayuda a reducir sentencias if else
 */
public class MainState {

    public static void main(String[] args) {
        MobileAlertStateContex contex = new MobileAlertStateContex();
        contex.alert();
        contex.alert();

        contex.setState( new Vibration() );
        contex.alert();
        contex.alert();

        contex.setState( new Silent() );
        contex.alert();
        contex.alert();

    }
}
