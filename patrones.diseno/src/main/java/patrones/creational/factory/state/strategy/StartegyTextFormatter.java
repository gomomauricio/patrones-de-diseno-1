package patrones.creational.factory.state.strategy;

public interface StartegyTextFormatter {
    void format(String text);
}
