package patrones.creational.factory.singleton;

/*
   Una aplicacion solo puede tener una instancia.
 */
public class MainSingleton
{
    public static void main(String[] args) {
        probarSingleton();
    }

    private static void probarSingleton()
    {
        Card.getINSTANCE().setCardNumber("123 456");

        System.out.println(" --> " + Card.getINSTANCE() );
    }




}
