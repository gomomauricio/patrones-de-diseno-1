package patrones.creational.factory.memento;

/**
 * Restaura el estado de un objeto a un estado anterior
 *
 */
public class MainMemento {

    public static void main(String[] args) {

        Carataker carateker = new Carataker();

        Article article = new Article("Alberto","Memento es una pelicula");

        article.setTexto(article.getTexto() + " de Nolan ");
        article.setResumen( " Batman regresa" );
        System.out.println("1.-" + article);

        carateker.addMemento(article.crearMemento());

        article.setTexto(article.getTexto() + " protagoniza el wason ");
        article.setResumen( " Batman por siempre" );
        System.out.println("2.-" + article);

        carateker.addMemento(article.crearMemento());

        article.setTexto(article.getTexto() + " y alguien mas");
        System.out.println("3.-" + article);

        ArticleMemento memento1 = carateker.getMemento(0);
        ArticleMemento memento2 = carateker.getMemento(1);

        article.restaurarMemento(memento1);
        System.out.println(" restaurado1 = " + article.getTexto());

        article.restaurarMemento(memento2);
        System.out.println(" restaurado2 = " + article.getTexto());

    }
}
