package patrones.creational.factory.iterator;

public class Card {

    String type;

    public Card(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



}
