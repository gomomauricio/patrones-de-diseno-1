package patrones.creational.factory.builder;

/*Este patron puede crear objetos complejos de un objeto fuente
 */
public class MainBuilder {

    public static void main(String[] args) {
        probarBuilder();
    }

    private static void probarBuilder()
    {
        Card ca = new Card.CardBuilder("Visa","0001 0002 0003 0004")
                .name("Alberto")
                .expires("2030")
                .credit(true)
                .build();

        System.out.println(ca);

        Card ca2 = new Card.CardBuilder("Amex","0005 0006 0007 0008")
                .name("Luis")
                .expires("2028")
                .credit(false)
                .build();

        System.out.println(ca2);

    }

}
