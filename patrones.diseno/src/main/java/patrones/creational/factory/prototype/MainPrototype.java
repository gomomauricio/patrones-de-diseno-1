package patrones.creational.factory.prototype;

import static patrones.creational.factory.prototype.PrototypeFactory.CardType.AMEX;
import static patrones.creational.factory.prototype.PrototypeFactory.CardType.VISA;

/*Crea objetos apartir de un modelo, permite que un objeto cree una copia de si mismo

 */
public class MainPrototype {

    public static void main(String[] args) {
        probarPrototype();
    }

    private static void probarPrototype()
    {

        PrototypeFactory.loadCard();
        try {
            PrototypeCard visa = PrototypeFactory.getInstance(VISA);
            visa.getCard();

            PrototypeCard amex = PrototypeFactory.getInstance(AMEX);
            amex.getCard();
        }
        catch (CloneNotSupportedException ce)
        {
            ce.printStackTrace();
        }
    }

}
