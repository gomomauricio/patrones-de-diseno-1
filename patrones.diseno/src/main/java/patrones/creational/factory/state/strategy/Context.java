package patrones.creational.factory.state.strategy;

public class Context {

    private StartegyTextFormatter startegyTextFormatter;

    public Context(StartegyTextFormatter startegyTextFormatter)
    {
        this.startegyTextFormatter = startegyTextFormatter;
    }

    public void publishText(String text)
    {
        startegyTextFormatter.format(text);
    }
}
