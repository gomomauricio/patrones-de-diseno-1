package patrones.creational.factory.state.strategy;

public class CapitalStartegyTextFormatter implements StartegyTextFormatter
{

    @Override
    public void format(String text) {
        System.out.println("Texto en MAYUSCULAS  >> " + text.toUpperCase());
    }
}
