package patrones.creational.factory.decorator;

public class SecureDecorator extends CreditDecorator{

    public SecureDecorator(Credit decoratorCredit) {
        super(decoratorCredit);
    }

    @Override
    public void showCredit()
    {
        decoratorCredit.showCredit();
        ConfigSecure();
    }

    private void ConfigSecure()
    {
        System.out.println("La tarjeta ha sido configurada con seguridad MAXIMA");
    }
}
