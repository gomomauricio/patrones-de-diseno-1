package patrones.creational.factory.adapter;

public interface Payment {

    void pay(String type);
}
