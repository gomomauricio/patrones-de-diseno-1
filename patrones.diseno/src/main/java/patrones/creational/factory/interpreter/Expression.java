package patrones.creational.factory.interpreter;

public interface Expression {

    boolean interpret(String context);
}
