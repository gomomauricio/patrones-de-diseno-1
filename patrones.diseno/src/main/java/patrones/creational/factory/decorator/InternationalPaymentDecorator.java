package patrones.creational.factory.decorator;

public class InternationalPaymentDecorator extends  CreditDecorator
{

    public InternationalPaymentDecorator(Credit decoratorCredit) {
        super(decoratorCredit);
    }

    @Override
    public void showCredit()
    {
        decoratorCredit.showCredit();
        ConfigInternationalPayment();
    }

    private void ConfigInternationalPayment()
    {
        System.out.println("La tarjeta ha sido configurada para hacer pagos internacionales");
    }
}
