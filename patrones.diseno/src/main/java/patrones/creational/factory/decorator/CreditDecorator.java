package patrones.creational.factory.decorator;

public class CreditDecorator implements  Credit{

    protected  Credit decoratorCredit;

    public CreditDecorator(Credit decoratorCredit) {
        this.decoratorCredit = decoratorCredit;
    }

    @Override
    public void showCredit() {
                decoratorCredit.showCredit();
    }
}
