package patrones.creational.factory.method;

public class Visa extends Payment{
    @Override
    void initialize() {
        System.out.println("Inicializando el pago Visa");
    }

    @Override
    void startPayment() {
        System.out.println("Realizando el pago con Visa");
    }

    @Override
    void endPayment() {
        System.out.println("Finalizado el pago con los servidores de Visa");
    }
}
