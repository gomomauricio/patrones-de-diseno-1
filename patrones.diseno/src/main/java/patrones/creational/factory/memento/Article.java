package patrones.creational.factory.memento;

public class Article {

    private String autor;
    private String texto;
    private String resumen;

    public Article(String autor, String texto)
    {
        this.autor = autor;
        this.texto = texto;
    }

    public ArticleMemento crearMemento()
    {
        ArticleMemento memento = new ArticleMemento(autor,texto);

        return memento;
    }

    public void restaurarMemento(ArticleMemento memento)
    {
        this.autor = memento.getAutor();
        this.texto = memento.getTexto();
    }


    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    @Override
    public String toString() {
        return "Article{" +
                "autor='" + autor + '\'' +
                ", texto='" + texto + '\'' +
                ", resumen='" + resumen + '\'' +
                '}';
    }
}
