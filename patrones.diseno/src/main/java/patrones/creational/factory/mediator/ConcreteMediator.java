package patrones.creational.factory.mediator;

public class ConcreteMediator implements Mediator {

    private ConcreteCollage1 user1;
    private ConcreteCollage2 user2;

    public void setUser1(ConcreteCollage1 collage1)
    {
        user1 = collage1;
    }

    public void setUser2(ConcreteCollage2 collage2)
    {
        user2 = collage2;
    }


    @Override
    public void send(String message, Collage collage) {

        if(collage == user1)
        {
            user2.messageReceived(message);
        }
        else if (collage == user2)
        {
            user1.messageReceived(message);
        }
    }


}
