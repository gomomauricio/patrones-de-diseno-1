package patrones.creational.factory.chain;

/*
Permite que varias clases manejen una solicitud
hay que implementar bien
es diificil de debugear
 */

public class MainChainOfResponsibility
{

    public static void main(String[] args) {
        probarChain();


    }

    private static void probarChain()
    {
        Card card = new Card();
        card.crediCardRequest(100002);
    }
}


