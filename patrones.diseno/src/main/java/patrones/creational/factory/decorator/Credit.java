package patrones.creational.factory.decorator;

public interface Credit {

    void showCredit();
}
