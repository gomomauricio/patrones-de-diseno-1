package patrones.creational.factory.state.strategy;

public class LowerStrategyTestFormatter implements StartegyTextFormatter
{


    @Override
    public void format(String text) {
        System.out.println("Texto en minusculas >> " + text.toLowerCase());
    }
}
