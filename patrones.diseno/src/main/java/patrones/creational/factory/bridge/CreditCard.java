package patrones.creational.factory.bridge;

public abstract class CreditCard {

    protected ICreditCard tarjeta;
    protected CreditCard(ICreditCard tarjeta)
    {
        this.tarjeta = tarjeta;
    }

    public abstract void realizarPago();
}
