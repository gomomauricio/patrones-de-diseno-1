package patrones.creational.factory.proxy;

public interface Internet {
    void connectTo(String url) throws Exception;
}
