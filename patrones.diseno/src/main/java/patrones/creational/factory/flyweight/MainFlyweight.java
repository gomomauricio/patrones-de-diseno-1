package patrones.creational.factory.flyweight;

import java.util.Random;

/**
 *
 * Actua como un objeto compartido, util cuando se necesita muchos objetos similares
 * nos ayuda a optimizar la memoria
 */
public class MainFlyweight {

    private static String[] enemyType = {"Private","Detective"};

    private static String[] weapon = {"Fusil","Revolver","Pistola","Metralleta","Lanza Granadas","9mm"};

    public static void main(String[] args) {

        for ( int i = 0; i<15; i++)
        {
            Enemy enemy = EnemyFactory.getEnemy( getRandomEnemyType());
            enemy.setWeapon( getRandomWeapon() );
            enemy.lifePoints();
        }


    }

    private static String getRandomWeapon()
    {
        Random r = new Random();

        int ranInt = r.nextInt(weapon.length);

        return weapon[ranInt];
    }

    private static String getRandomEnemyType()
    {
        Random r = new Random();

        int ranInt = r.nextInt(enemyType.length);

        return enemyType[ranInt];
    }
}
