package patrones.creational.factory.observer;

public class Semaforo {

    public String estatus;

    public Semaforo(String estatus)
    {
        this.estatus = estatus;
    }

    public String getEstatus()
    {
        return estatus;
    }
}
