package patrones.creational.factory.abstractt;

public class GooglePayment implements Payment {

    public void doPayment() {
        System.out.println("Pagando con google payment");
    }
}
