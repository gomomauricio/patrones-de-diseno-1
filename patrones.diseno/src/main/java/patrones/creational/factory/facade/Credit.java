package patrones.creational.factory.facade;

public interface Credit {

    void showCredit();
}
