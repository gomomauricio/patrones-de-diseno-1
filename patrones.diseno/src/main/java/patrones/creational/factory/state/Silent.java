package patrones.creational.factory.state;

public class Silent implements MobileAlertState{
    @Override
    public void alert(MobileAlertStateContex contex) {
        System.out.println("Silencio . . . Pantalla Iluminada . . . ");
    }
}