package patrones.creational.factory.mediator;

public class ConcreteCollage2 extends Collage
{
    public ConcreteCollage2(Mediator mediator)
    {
        super(mediator);
    }

    @Override
    public void send(String message) {
        mediator.send(message,this);
    }

    @Override
    public void messageReceived(String message) {
        System.out.println("Colleage 2 ha recibido el siguiente mensaje:  " + message);
    }
}