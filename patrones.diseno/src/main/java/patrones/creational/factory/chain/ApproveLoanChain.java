package patrones.creational.factory.chain;

public interface ApproveLoanChain {

    void setNext(ApproveLoanChain loan);

    ApproveLoanChain getNext();

    void crediCardRequest(int totalLoan);
}
