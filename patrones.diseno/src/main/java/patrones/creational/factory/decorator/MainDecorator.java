package patrones.creational.factory.decorator;

/*
agrega comportamientos adicionales sin alterar las demas clases
 */
public class MainDecorator {
    public static void main(String[] args) {

        Credit gold = new Gold();
        Credit blackInternationalPayment = new Black();

        blackInternationalPayment = new InternationalPaymentDecorator( blackInternationalPayment );

        Credit goldSecureInternational = new Gold();
        goldSecureInternational=new InternationalPaymentDecorator( goldSecureInternational );
        goldSecureInternational = new SecureDecorator( goldSecureInternational );

        System.out.println("*********** Tarejeta gold con configuracion ***********");
        gold.showCredit();

        System.out.println("*********** Tarejeta black con configuracion ***********");
        blackInternationalPayment.showCredit();

        System.out.println("*********** Tarejeta gold2 con configuracion ***********");
        goldSecureInternational.showCredit();

    }
}
