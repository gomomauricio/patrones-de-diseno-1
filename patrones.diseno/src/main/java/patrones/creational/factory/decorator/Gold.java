package patrones.creational.factory.decorator;

public class Gold implements Credit {
    @Override
    public void showCredit() {
        System.out.println("El credito es de 1 000");
    }
}
