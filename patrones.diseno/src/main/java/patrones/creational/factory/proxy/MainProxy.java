package patrones.creational.factory.proxy;

/*
Intermediario para acceder algun objeto, actua como controlador a dicho objeto
 */
public class MainProxy
{
    public static void main(String[] args) {

        Internet internet = new ProxyInternet();

        try {
            internet.connectTo("udemy.com");
            internet.connectTo("facebook.com");
            internet.connectTo("att.com");
        }
        catch (Exception ex)
        {
            System.out.println( "ERROR --> " + ex.getMessage());
        }

    }
}
