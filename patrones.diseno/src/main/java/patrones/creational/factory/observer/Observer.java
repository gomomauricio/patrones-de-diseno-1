package patrones.creational.factory.observer;

public interface Observer {

    void update(Semaforo semaforo);
}
