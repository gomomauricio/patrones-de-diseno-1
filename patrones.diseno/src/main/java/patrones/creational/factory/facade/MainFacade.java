package patrones.creational.factory.facade;
/*
Busca simplificar el sistema para el cliente
 */
public class MainFacade {
    public static void main(String[] args) {

        CreditMarket creditMarket = new CreditMarket();
        creditMarket.showCreditGold();
        creditMarket.showCreditSilver();
        creditMarket.showCreditBlack();
    }
}
