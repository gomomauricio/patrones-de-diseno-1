package patrones.creational.factory.composite;

import java.util.ArrayList;
import java.util.List;

public class CuentaComposite implements CuentaComponent {

    private List<CuentaComponent> childCuentas;

    public CuentaComposite()
    {
        this.childCuentas = new ArrayList<>();
    }


    @Override
    public void showAccountName() {
        childCuentas.forEach( n -> n.showAccountName());
    }

    @Override
    public Double getAmount() {
        Double totalAmount = 0.0;

        totalAmount =  childCuentas.stream()
                                   .mapToDouble( CuentaComponent :: getAmount )
                                   .sum();

        return totalAmount;
    }


    public void addCuenta(CuentaComponent cuenta)
    {
        childCuentas.add(cuenta);
    }

    public void removeCuenta(CuentaComponent cuenta)
    {
        childCuentas.remove(cuenta);
    }

}
