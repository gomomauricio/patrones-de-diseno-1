package patrones.creational.factory.abstractt;

public interface Payment {

    void doPayment();
}
