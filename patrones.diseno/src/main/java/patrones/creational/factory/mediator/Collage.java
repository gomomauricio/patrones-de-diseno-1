package patrones.creational.factory.mediator;

public abstract class Collage
{
    protected Mediator mediator;

    public Collage(Mediator mediator)
    {
        this.mediator = mediator;
    }

    public abstract void send(String message);

    public abstract void messageReceived(String message);


}
