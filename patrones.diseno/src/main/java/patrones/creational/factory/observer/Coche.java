package patrones.creational.factory.observer;

public class Coche implements Observer
{

    @Override
    public void update(Semaforo semaforo) {
        if(semaforo.estatus.equals("ROJO COCHE"))
        {
            System.out.println("Semaforo COCHE verde: coche puede pasar");
        }else
        {
            System.out.println("Semaforo COCHE rojo: coche NO puede pasar");
        }
    }
}
