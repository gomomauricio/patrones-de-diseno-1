package patrones.creational.factory.iterator;

/*
 permite recorrer una coleccion de objetos
 */

public class MainIterator {

    public static void main(String[] args) {
        Card[] cards = new Card[5];

        cards[0] = new Card("VISA");
        cards[1] = new Card("AMEX");
        cards[2] = new Card("BBVA");
        cards[3] = new Card("HSBC");
        cards[4] = new Card("MASTER");

        List list = new CardList(cards);

        Iterator iterator = list.iterator();

        while( iterator.hasNext() )
        {
            Card tarjeta = (Card) iterator.next();
            System.out.println( tarjeta.getType() );
        }


    }
}
