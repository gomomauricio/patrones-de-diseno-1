package patrones.creational.factory.state;

public class Sound implements MobileAlertState{
    @Override
    public void alert(MobileAlertStateContex contex) {
        System.out.println("Alert . . . Ring ------  Ring . . . ");
    }
}