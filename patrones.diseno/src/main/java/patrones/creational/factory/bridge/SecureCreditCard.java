package patrones.creational.factory.bridge;

public class SecureCreditCard implements ICreditCard
{

    @Override
    public void realizarPago() {
        System.out.println("Pago realizado CON SEGURIDAD");
    }
}
