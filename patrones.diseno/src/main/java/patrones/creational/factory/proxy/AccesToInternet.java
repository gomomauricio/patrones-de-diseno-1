package patrones.creational.factory.proxy;

public class AccesToInternet implements Internet{


    @Override
    public void connectTo(String url) throws Exception {
        System.out.println("Conectando a " + url);
    }
}
