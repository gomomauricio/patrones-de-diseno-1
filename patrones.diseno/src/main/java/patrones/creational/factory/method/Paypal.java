package patrones.creational.factory.method;

public class Paypal extends  Payment{


    @Override
    void initialize() {
        System.out.println("Inicializando el pago");
    }

    @Override
    void startPayment() {
        System.out.println("Realizando el pago con paypal");
    }

    @Override
    void endPayment() {
        System.out.println("Finalizado el pago con los servidores de paypal");
    }
}
