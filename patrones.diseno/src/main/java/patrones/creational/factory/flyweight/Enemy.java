package patrones.creational.factory.flyweight;

public interface Enemy
{
    void setWeapon(String weapon);

    void lifePoints();


}
