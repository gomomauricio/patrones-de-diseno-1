package patrones.creational.factory.abstractt;

public class CardPayment implements  Payment{
    public void doPayment() {
        System.out.println("Pagando Con Tarjeta De Credito");
    }
}
