package patrones.creational.factory.visitor;

public class ClassicCreditCardVisitor implements CreditCardVisitor
{
    @Override
    public void ofertaGasolina(OfertaGasolina gasolina) {
        System.out.println("Descuento del 3% de gasolina con tu tarjeta clasica");
    }

    @Override
    public void ofertaVuelos(OfertaVuelos vuelos) {
        System.out.println("Descuento del 5% de vuelos con tu tarjeta clasica");
    }
}
