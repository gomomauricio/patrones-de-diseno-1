package patrones.creational.factory.iterator;

import patrones.creational.factory.command.CreditCardActivateCommand;

public class CardList implements List
{
    private Card[] cards;

    public CardList(Card[] cards) {
        this.cards = cards;
    }

    @Override
    public Iterator iterator() {
        return new CardIterator(cards);
    }
}
