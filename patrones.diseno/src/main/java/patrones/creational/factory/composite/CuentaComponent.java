package patrones.creational.factory.composite;

public interface CuentaComponent {

    void showAccountName();
    Double getAmount();
}
