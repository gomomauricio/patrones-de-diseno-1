package patrones.creational.factory.chain;

//Esta clase sera el punto de entrada
//gestionara las tarjetas
public class Card implements  ApproveLoanChain{

    private ApproveLoanChain next;

    @Override
    public void setNext(ApproveLoanChain loan) {
        next = loan;
    }

    @Override
    public ApproveLoanChain getNext() {
        return next;
    }

    //aplicamos la logica de la cadena de responsabilidad
    @Override
    public void crediCardRequest(int totalLoan) {

      //  le indicamos quien gestiona primero
        Gold gold = new Gold();
        this.setNext(gold);

        Platinium plat = new Platinium();
        gold.setNext(plat);

        Black black = new Black();
        plat.setNext(black);


        next.crediCardRequest(totalLoan);
    }
}
